Welcome to a public repository for Alice3 a3c classes to extend functionality in Alice 3 programming language available at Alice.org. 

To promote ‪#‎HourOfCode‬ using Alice Intro to Computer Programming​ which I think is awesome.  I have created a Public Code Repository for Alice 3 class files being created to facilitate sharing.  Just create a free bitbucket.org account and search for "alice3" in Find a Repository.

# How to contribute to repository? #
If you want to push a improved version of existing file in repository or want to contribute a new file, join this Alice3_Class_Library Public Repository.  Any questions, email me at carlhub@gmail.com.

# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact